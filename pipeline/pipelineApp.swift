//
//  pipelineApp.swift
//  pipeline
//
//  Created by Francisco Santana Cardoso on 29/11/22.
//

import SwiftUI

@main
struct pipelineApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
