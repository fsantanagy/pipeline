//
//  Calculator.swift
//  pipeline
//
//  Created by Francisco Santana Cardoso on 29/11/22.
//

import Foundation
enum OperationTypeEnum {
    case sum
    case subtraction
    case divisiondivision
    case multiplication
}

class Calculator {
    func calculate(numberOne: Int, numberTwo: Int, operation: OperationTypeEnum) -> Int {
        switch operation {
        case .sum:
            return numberOne + numberTwo
        case .subtraction:
            return numberOne - numberTwo
        case .divisiondivision:
            return numberOne / numberTwo
        case .multiplication:
            return numberOne * numberTwo
        }
    }
}
