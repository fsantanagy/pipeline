//
//  CalculatorTest.swift
//  pipelineTests
//
//  Created by Francisco Santana Cardoso on 29/11/22.
//

import XCTest
@testable import pipeline

final class CalculatorTest: XCTestCase {
    var sut: Calculator?
    
    override func setUp() async throws {
        sut = Calculator()
    }
    
    func testCalculateSum() {
        let result = sut?.calculate(numberOne: 1, numberTwo: 1, operation: .sum)
        XCTAssert(result == 2)
    }
    
    func testCalculateSubtraction() {
        let result = sut?.calculate(numberOne: 2, numberTwo: 1, operation: .subtraction)
        XCTAssert(result == 1)
    }
    
    func testCalculateDivisiondivision() {
        let result = sut?.calculate(numberOne: 6, numberTwo: 2, operation: .divisiondivision)
        XCTAssert(result == 3)
    }
    
    func testCalculateMultiplication() {
        let result = sut?.calculate(numberOne: 2, numberTwo: 3, operation: .multiplication)
        XCTAssert(result == 6)
    }
    
}
